<?php

$plugin = array(
  'title' => t('Rendered entityform type'),
  'description' => t('Rendered entityform type.'),
  'category' => t('Entityform Type'),
  'defaults' => array('entityform_type' => '', 'view_mode' => 'full'),
);

function entityform_type_panels_view_content_type_edit_form($form, &$form_state) {
  $entityform_types = entityform_get_types();
  $conf = $form_state['conf'];
  $entity_info = entity_get_info('entityform_type');

  $entityform_type_options = array();
  if (!empty($entityform_types)) {
    foreach ($entityform_types as $entityform_type) {
      $entityform_type_options[$entityform_type->type] = $entityform_type->label;
    }
  }

  $view_mode_options = array();
  if (!empty($entity_info['view modes'])) {
    foreach ($entity_info['view modes'] as $mode => $settings) {
      $view_mode_options[$mode] = $settings['label'];
    }
  }

  $form['entityform_type'] = array(
    '#type' => 'select',
    '#options' => $entityform_type_options,
    '#title' => t('Entityform type'),
    '#default_value' => $conf['entityform_type'],
    '#required' => TRUE,
  );

  if (count($view_mode_options) > 1) {
    $form['view_mode'] = array(
      '#type' => 'select',
      '#options' => $view_mode_options,
      '#title' => t('View mode'),
      '#default_value' => $conf['view_mode'],
    );
  }
  else {
    $form['view_mode_info'] = array(
      '#type' => 'item',
      '#title' => t('View mode'),
      '#description' => t('Only one view mode is available for this entity type.'),
      '#markup' => $view_mode_options ? current($view_mode_options) : t('Default'),
    );

    $form['view_mode'] = array(
      '#type' => 'value',
      '#value' => $view_mode_options ? key($view_mode_options) : 'default',
    );
  }

  return $form;
}

function entityform_type_panels_view_content_type_edit_form_submit(&$form, &$form_state) {
  foreach (array_keys($form_state['plugin']['defaults']) as $key) {
    $form_state['conf'][$key] = $form_state['values'][$key];
  }
}

function entityform_type_panels_view_content_type_render($subtype, $conf, $args, $context) {
  $block = new stdClass();

  $block->content = entity_view('entityform_type', array($conf['entityform_type'] => entityform_get_types($conf['entityform_type'])), $conf['view_mode']);

  return $block;
}
